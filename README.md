# About this Code

The code in this repository is part of a tutorial by **0612 TV** entitled **Operator Overloading - Friday Minis 247**, hosted on YouTube.

[![Click to Watch](https://img.youtube.com/vi/99p_oayshfQ/0.jpg)](https://www.youtube.com/watch?v=99p_oayshfQ "Click to Watch")


# License

This project is licensed under the **Apache License 2.0**. For more details, please refer to LICENSE.txt within this repository.