#include <iostream>
using namespace std;

class Point {
	private:
		int x;
		int y;
	public:
		Point(int xPos, int yPos) {
			x = xPos;
			y = yPos;
		}
		int getX() { return x; }
		int getY() { return y; }
		
		// Method 2
		Point operator+ (Point second) {
			int newX = this->getX() + second.getX();
			int newY = this->getY() + second.getY();
			return Point(newX, newY);
		}
};

// Method 1
// Point operator+ (Point first, Point second) {
	// int newX = first.getX() + second.getX();
	// int newY = first.getY() + second.getY();
	// return Point(newX, newY);
// }

int main() {
	Point p1 = Point(2,3);
	Point p2 = Point(1,4);
	
	Point ans = p1 + p2;
	cout << "The point is at (" << ans.getX() << "," << ans.getY() << ")" << endl;
	return 0;
}